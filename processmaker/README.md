![ProcessMaker logo](http://www.processmaker.com/themes/processmaker/images/logo.jpg "ProcessMaker")

[![Build Status](https://travis-ci.org/colosa/processmaker.png?branch=michelangelo)](http://travis-ci.org/colosa/processmaker)

README
======

ProcessMaker "Michelangelo"
----------------

***(It is under development yet, please wait for the final release, that is comming soon)***


Overview
--------

ProcessMaker is an open source, workflow management software suite, which
includes tools to automate your workflow, design forms, create documents, assign
roles and users, create routing rules, and map an individual process quickly and
easily. It's relatively lightweight and doesn't require any kind of installation
on the client computer. This file describes the requirements and installation
steps for the server.

License
-------

ProcessMaker - Automate your Workflow
Copyright (C) 2002 - 2013 Colosa Inc.

Licensed under the terms of the GNU Affero General Public License version 3:
http://www.affero.org/oagpl.html

For further information visit:
http://www.processmaker.com/

