var socket = io();

var actualizaLog = function (data) {
    $('.list-group-item').removeClass("active");
    $('#miLog').append('<a href="#" class="list-group-item active">' + data.action + '</a>');
};

var actualizaTabla = function (data) {
    $('#tablaUser').html('');
    for (var i=0; i<data.length; i++) {
        var html = '';
        html += '<tr>';
        html += '<td>' + data[i].userName + '</td>';
        html += '<td>' + data[i].firstName + '</td>';
        html += '<td>' + data[i].lastName + '</td>';
        html += '<td>' + data[i].password + '</td>';
        html += '<td>';
        html += '<div class="controls center">';
        html += '<button type="button" class="btn btn-danger btn-xs" onclick="borrarUser(\''+ data[i]._id +'\'); return false;">Borrar</button> ';
        html += '</div>';
        html += '</td>';
        html += '</tr>';
        $('#tablaUser').append(html);
    }
};

socket.on('connect', function(){});
socket.on('event', function(data){});
socket.on('disconnect', function(){});

socket.on('canal clientes', function(data){
    actualizaLog(data);
});

socket.on('canal tabla', function(data){
    actualizaTabla(data);
    $('#searchId').val('');
});


var guardarNuevo = function () {
    var data = {
        "userName"  : $('#usernameText').val(),
        "firstName" : $('#firstnameText').val(),
        "lastName"  : $('#lastnameText').val(),
        "password"  : $('#passowordText').val()
    };
    $.ajax({
        type    : "POST",
        headers : {"Content-Type": "application/json"},
        dataType: "json",
        url     : "/user",
        data    : JSON.stringify(data),
        success : function(data, status) {
            $('#usernameText').val('');
            $('#firstnameText').val('');
            $('#lastnameText').val('');
            $('#passowordText').val('');
        },
        error: function (xhr, desc, err) { console.log("Error: " + err); }
    });
};

var borrarUser = function (id) {
    $.ajax({
        type    : "DELETE",
        headers : {"Content-Type": "application/json"},
        dataType: "json",
        url     : "/user/"+id,
        success : function(data, status) {},
        error: function (xhr, desc, err) { console.log("Error: " + err); }
    });
};

var searchUser = function () {
    $.ajax({
        type    : "GET",
        headers : {"Content-Type": "application/json"},
        dataType: "json",
        url     : "/users/"+$('#searchId').val(),
        success : function(data, status) {
            actualizaTabla(data);
        },
        error: function (xhr, desc, err) { console.log("Error: " + err); }
    });
};

$('#guardarNuevo').click(guardarNuevo);
$('#searchUser').click(searchUser);


