var express = require("express");
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var mongo = require('mongoskin');
var db = mongo.db("mongodb://192.168.11.54:27017/cochalo");
var colCategory = db.collection('Category');
var colDocument = db.collection('Document');
var BSON = mongo.BSONPure;

var session = require("express-session");
var RedisStore = require("connect-redis")(session);
var cookieParser = require('cookie-parser');
var redisClient = require("redis").createClient();

var bodyParser = require('body-parser');
var statusError = 400;
    

app.use(cookieParser());
app.use(session({
    secret: "secreto",
    store: new RedisStore({
        client: redisClient
    }),
    cookie: {
        maxAge  : 24*60*60*1000
    },
    proxy: true,
    resave: true,
    saveUninitialized: true
}));


app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/bower_components'));
app.use(bodyParser.json({ type: 'application/json' }));


io.on('connection', function(socket) {
    /*
    guardarBoardPro({ip: socket.request.socket.remoteAddress, type: 'CONNECTED', action : 'SE A CONECTADO'}).then(function() {
        console.log("Se guardo correctamente en BOARD");
    }, function(err) {
        console.log("Existe un error al guardar ");
    });
    */
    socket.on('disconnect', function(valor){
        //guardarBoardPro({ip: socket.request.socket.remoteAddress, type: 'DISCONNECTED', action: "SE A DESCONECTADO", date: new Date()});
    });

    //cargarTablaPro();
});


/*
socket.on('canal clientes', function(msg){
    io.emit('canal clientes', {ip: socket.request.socket.remoteAddress, nombre: "cochalo", action: "cochalo", date: new Date()});
});
*/

app.get("/users", function (req, res)  {
    try {
        var filtros = new Array();
        
        if (typeof(req.query.firstName) != "undefined") {
            filtros.push({"firstName" : req.query.firstName});
        }
        if (typeof(req.query.userName) != "undefined") {
            filtros.push({"userName" : req.query.userName});
        }
        if (typeof(req.query.lastName) != "undefined") {
            filtros.push({"lastName" : req.query.lastName});
        }
        
        if (filtros.length != 0) {
            filtros = {$and:filtros};
        }
        colUsers.find(filtros).toArray( function(err, result) {
            if (err) {
                throw err;
            }
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            //io.emit('canal clientes', {ip: ip, action: "LISTO TO DOS LOS USUARIOS"});
            guardarBoardPro({ip: ip, type: 'SELECT ALL', action: "LISTO TODOS LOS USUARIOS", date: new Date()});
            res.send(result);
        });
    } catch (err) {
        console.log("Error:", err);
        res.status(statusError).send('Error: ' + err);
    }
});

app.get("/users/:search", function (req, res)  {
    try {
        colUsers.find({$or:[{"userName": eval('/.*'+req.params.search+'.*/')},{"firstName": eval('/.*'+req.params.search+'.*/')},{"lastName": eval('/.*'+req.params.search+'.*/')}]}).toArray( function(err, result) {
            if (err) {
                throw err;
            }
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            guardarBoardPro({ip: ip, type: 'SEARCH', action: "LISTO UNA BUSQUEDA", date: new Date()});
            //io.emit('canal clientes', {ip: ip, action: "LISTO UNA BUSQUEDA"});
            res.send(result);
        });
    } catch (err) {
        console.log("Error:", err);
        res.status(statusError).send('Error: ' + err);
    }
});

app.get("/user/:id", function (req, res)  {
    try {
        colUsers.find({"_id": new BSON.ObjectID(req.params.id)}).toArray( function(err, result) {
            if (err) {
                throw err;
            }
            if (result.length == 0) {
                res.status(statusError).send('Error: Ese Id no corresponde a un usuario actual');
            } else {
                var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                //io.emit('canal clientes', {ip: ip, accion: "LISTO AL USUARIO: " + result[0].firstName});
                guardarBoardPro({ip: ip, type: 'SELECT USER', action: "LISTO AL USUARIO: " + result[0].firstName, date: new Date()});
                res.send(result);
            }
        });
    } catch (err) {
        console.log("Error:", err);
        res.status(statusError).send('Error: Ese Id no corresponde a un usuario actual');
    }
});

app.post("/user", function (req, res)  {
    try {
        colUsers.insert({userName: req.body.userName, firstName: req.body.firstName, lastName: req.body.lastName, password: md5(req.body.password)}, function(err, result) {
            if (err) {
                throw err;
            }

            //if (result) console.log('Added!');
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            //io.emit('canal clientes', {ip: ip, action: "AÑADIO UN USUARIO"});
            guardarBoardPro({ip: ip, type: 'CREATE', action: "AÑADIO AL USUARIO: " + result[0].firstName, date: new Date()});
            cargarTablaPro();
            res.send(result);
        });
    } catch (err) {
        console.log("Error:", err);
        //res.sendStatus(400);
        res.status(statusError).send('Error: ' + err);
    }
});


app.put("/user/:id", function (req, res)  {
    try {
        var cade = {};
        if (typeof(req.body.userName) != 'undefined') {
            cade.userName = req.body.userName;
        }
        if (typeof(req.body.lastName) != 'undefined') {
            cade.lastName = req.body.lastName;
        }
        if (typeof(req.body.firstName) != 'undefined') {
            cade.firstName = req.body.firstName;
        }
        if (typeof(req.body.password) != 'undefined') {
            cade.password = md5(req.body.password);
        }
        colUsers.update({"_id": new BSON.ObjectID(req.params.id)}, {'$set':cade}, function(err) {
            if (err) throw err;
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            guardarBoardPro({ip: ip, type: 'UPDATE', action: "ACTUALIZADO AL USUARIO: " + req.params.id, date: new Date()});
            cargarTablaPro();
            res.sendStatus(200);
        });
    } catch (err) {
        console.log("Error:", err);
        res.status(statusError).send('Error: Ese Id no corresponde a un usuario actual');
    }
});


app.delete("/user/:id", function (req, res)  {
    try {
        var nameUser;
        colUsers.find({"_id": new BSON.ObjectID(req.params.id)}).toArray( function(err, result) {
            if (err) {
                throw err;
            }
            if (result.length == 0) {
                res.status(statusError).send('Error: Ese Id no corresponde a un usuario actual');
            } else {
                nameUser = result[0].firstName;
                colUsers.remove({"_id": new BSON.ObjectID(req.params.id)}, function(err2, result2) {
                    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    //io.emit('canal clientes', {ip: ip, action: "BORRO AL USUARIO " + nameUser});
                    guardarBoardPro({ip: ip, type: 'DELETE', action: "BORRO AL USUARIO: " + nameUser, date: new Date()});
                    cargarTablaPro();
                    res.sendStatus(200);
                });
            }
        });
    } catch (err) {
        console.log("Error:", err);
        res.status(statusError).send('Error: Ese Id no corresponde a un usuario actual');
    }
});





server.listen(3000);




/*
app.listen(3000, function() {
    console.log("Servidor corriendo en el puerto 3000");
});
*/